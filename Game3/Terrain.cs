﻿using Bowling.Cameras;
using Bowling.Materials;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling
{
    public class Terrain
    {
        VertexPositionNormalTexture[] vertices; // Vertex array
        VertexBuffer vertexBuffer; // Vertex buffer
        int[] indices; // Index array
        IndexBuffer indexBuffer; // Index buffer
        float[,] heights; // Array of vertex heights
        float height; // Maximum height of terrain
        float cellSize; // Distance between vertices on x and z axes
        int width = 30, length = 40; // Number of vertices on x and z axes
        int nVertices, nIndices; // Number of vertices and indices
        Effect effect; // Effect used for rendering
        GraphicsDevice GraphicsDevice; // Graphics device to draw with
        Texture2D floorTexture; // Heightmap texture

        public Material Material { get; set; }


        public Terrain(Effect effect, float CellSize, float Height, GraphicsDevice GraphicsDevice, Camera camera, Texture2D floorTexture)
        {
            this.floorTexture = floorTexture;
            cellSize = CellSize;
            height = Height;
            this.GraphicsDevice = GraphicsDevice;
            // 1 vertex per pixel
            nVertices = width * length;
            // (Width-1) * (Length-1) cells, 2 triangles per cell, 3 indices per
            // triangle
            nIndices = (width - 1) * (length - 1) * 6;
            vertexBuffer = new VertexBuffer(GraphicsDevice,
            typeof(VertexPositionNormalTexture), nVertices,
            BufferUsage.WriteOnly);
            indexBuffer = new IndexBuffer(GraphicsDevice,
            IndexElementSize.ThirtyTwoBits,
            nIndices, BufferUsage.WriteOnly);
            getHeights();
            createVertices();
            createIndices();
            genNormals();
            vertexBuffer.SetData<VertexPositionNormalTexture>(vertices);
            indexBuffer.SetData<int>(indices);
        }

        public void Draw(Camera camera)
        {
            var world = Matrix.Identity * Matrix.CreateScale(2f);

            if (effect is BasicEffect basicEffect)
            {
                basicEffect.World = world;
                basicEffect.View = camera.View;
                basicEffect.Projection = camera.Projection;
                basicEffect.VertexColorEnabled = false;
                basicEffect.TextureEnabled = true;
                basicEffect.Texture = floorTexture;
            }
            else
            {
                SetEffectParameter(effect, "World", world);
                SetEffectParameter(effect, "View", camera.View);
                SetEffectParameter(effect, "Projection", camera.Projection);
                SetEffectParameter(effect, "CameraPosition", camera.Position);
                SetEffectParameter(effect, "TextureEnabled", true);
                SetEffectParameter(effect, "BasicTexture", floorTexture);
                SetEffectParameter(effect, "DiffuseColor", Color.White.ToVector3());
                SetEffectParameter(effect, "SpecularPower", 3000f);
            }

            Material.SetEffectParameters(effect);

            foreach (var pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                GraphicsDevice.SetVertexBuffer(vertexBuffer);
                GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleList, 0, vertexBuffer.VertexCount / 3);
            }
        }

        private void getHeights()
        {
            heights = new float[width, length];

            for (int y = 0; y < length; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    heights[x, y] = 0;
                }
            }
        }

        private void createVertices()
        {
            vertices = new VertexPositionNormalTexture[nVertices];
            // Calculate the position offset that will center the terrain at
            Vector3 offsetToCenter = -new Vector3(((float)width / 2.0f) *
            cellSize, 0, ((float)length / 2.0f) * cellSize);
            // For each pixel in the image
            for (int z = 0; z < length; z++)
                for (int x = 0; x < width; x++)
                {
                    // Find position based on grid coordinates and height in
                    // heightmap
                    Vector3 position = new Vector3(x * cellSize,
                    heights[x, z], z * cellSize) + offsetToCenter;  
                    // UV coordinates range from (0, 0) at grid location (0, 0) to
                    // (1, 1) at grid location (width, length)
                    Vector2 uv = new Vector2((float)x / width, (float)z / length);
                    // Create the vertex
                    vertices[z * width + x] = new VertexPositionNormalTexture(
                    position, Vector3.Zero, uv);
                }
        }
        private void createIndices()
        {
            indices = new int[nIndices];
            int i = 0;
            // For each cell
            for (int x = 0; x < width - 1; x++)
                for (int z = 0; z < length - 1; z++)
                {
                    // Find the indices of the corners
                    int upperLeft = z * width + x;
                    int upperRight = upperLeft + 1;
                    int lowerLeft = upperLeft + width;
                    int lowerRight = lowerLeft + 1;
                    // Specify upper triangle
                    indices[i++] = upperLeft;
                    indices[i++] = upperRight;
                    indices[i++] = lowerLeft;
                    // Specify lower triangle
                    indices[i++] = lowerLeft;
                    indices[i++] = upperRight;
                    indices[i++] = lowerRight;
                }
        }

        private void genNormals()
        {
            // For each triangle
            for (int i = 0; i < nIndices; i += 3)
            {
                // Find the position of each corner of the triangle
                Vector3 v1 = vertices[indices[i]].Position;
                Vector3 v2 = vertices[indices[i + 1]].Position;
                Vector3 v3 = vertices[indices[i + 2]].Position;
                // Cross the vectors between the corners to get the normal
                Vector3 normal = Vector3.Cross(v1 - v2, v1 - v3);
                normal.Normalize();
                // Add the influence of the normal to each vertex in the
                // triangle
                vertices[indices[i]].Normal += normal;
                vertices[indices[i + 1]].Normal += normal;
                vertices[indices[i + 2]].Normal += normal;
            }
            // Average the influences of the triangles touching each
            // vertex
            for (int i = 0; i < nVertices; i++)
                vertices[i].Normal.Normalize();
        }

        private void SetEffectParameter(Effect effect, string paramName, object val)
        {
            if (effect.Parameters[paramName] == null)
                return;
            if (val is Vector3)
                effect.Parameters[paramName].SetValue((Vector3)val);
            else if (val is bool)
                effect.Parameters[paramName].SetValue((bool)val);
            else if (val is Matrix)
                effect.Parameters[paramName].SetValue((Matrix)val);
            else if (val is Texture2D)
                effect.Parameters[paramName].SetValue((Texture2D)val);
            else if (val is float)
                effect.Parameters[paramName].SetValue((float)val);
        }

    }
}
