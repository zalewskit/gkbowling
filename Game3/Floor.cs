﻿using Bowling.Cameras;
using Bowling.Materials;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt4
{
    class Floor
    {
        public const int mapWidth = 7;
        public const int mapHeight = 11;


        GraphicsDevice device;
        VertexBuffer floorBuffer;

        Color[] floorColors = new Color[2] { Color.White, Color.Gray };

        #region textures

        Texture2D floorTexture;

        #endregion

        public Material Material { get; set; }

        public Floor(GraphicsDevice device, Texture2D floorTexture = null)
        {
            this.device = device;
            this.floorTexture = floorTexture;
            BuildFloorBuffer();
        }

        private void BuildFloorBuffer()
        {
            List<VertexPositionTexture> vertexList = new List<VertexPositionTexture>();

            int counter = 0;

            for(int x = 0; x < mapWidth; x++)
            {
                counter++;
                for(int z=0; z<mapHeight; z++)
                {
                    counter++;
                    foreach(var vertex in FloorTile(x, z, floorColors[counter %2]))
                    {
                        vertexList.Add(vertex);
                    }
                }
            }

            
            
            floorBuffer = new VertexBuffer(device, VertexPositionTexture.VertexDeclaration, vertexList.Count, BufferUsage.WriteOnly);
            floorBuffer.SetData(vertexList.ToArray());
        }

        private IEnumerable<VertexPositionTexture> FloorTile(int xOffset, int zOffset, Color color)
        {
            var vList = new List<VertexPositionTexture>()
            {
                new VertexPositionTexture(new Vector3(0 + xOffset, 0, 0 + zOffset), new Vector2(0, 0)),
                new VertexPositionTexture(new Vector3(1 + xOffset, 0, 0 + zOffset), new Vector2(1, 0)),
                new VertexPositionTexture(new Vector3(0 + xOffset, 0, 1 + zOffset), new Vector2(0, 1)),

                new VertexPositionTexture(new Vector3(1 + xOffset, 0, 0 + zOffset), new Vector2(1, 0)),
                new VertexPositionTexture(new Vector3(1 + xOffset, 0, 1 + zOffset), new Vector2(1, 1)),
                new VertexPositionTexture(new Vector3(0 + xOffset, 0, 1 + zOffset), new Vector2(0, 1)),
            };

            return vList;
        }

        public void Draw(Bowling.Cameras.Camera camera, Effect effect)
        {
            var world = Matrix.Identity * Matrix.CreateScale(2f);

            if (effect is BasicEffect basicEffect)
            {
                basicEffect.World = world ;
                basicEffect.View = camera.View;
                basicEffect.Projection = camera.Projection;
                basicEffect.VertexColorEnabled = false;
                basicEffect.TextureEnabled = true;
                basicEffect.Texture = floorTexture;
            }
            else
            {
                SetEffectParameter(effect, "World", world);
                SetEffectParameter(effect, "View", camera.View);
                SetEffectParameter(effect, "Projection", camera.Projection);
                SetEffectParameter(effect, "CameraPosition", camera.Position);
                SetEffectParameter(effect, "TextureEnabled", true);
                SetEffectParameter(effect, "BasicTexture", floorTexture);
                SetEffectParameter(effect, "DiffuseColor", Color.White.ToVector3());
                SetEffectParameter(effect, "SpecularPower", 3000f);
            }

            Material.SetEffectParameters(effect);

            foreach (var pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
                device.SetVertexBuffer(floorBuffer);
                device.DrawPrimitives(PrimitiveType.TriangleList, 0, floorBuffer.VertexCount / 3);
            }
        }

        private void SetEffectParameter(Effect effect, string paramName, object val)
        {
            if (effect.Parameters[paramName] == null)
                return;
            if (val is Vector3)
                effect.Parameters[paramName].SetValue((Vector3)val);
            else if (val is bool)
                effect.Parameters[paramName].SetValue((bool)val);
            else if (val is Matrix)
                effect.Parameters[paramName].SetValue((Matrix)val);
            else if (val is Texture2D)
                effect.Parameters[paramName].SetValue((Texture2D)val);
            else if (val is float)
                effect.Parameters[paramName].SetValue((float)val);
        }
    }
}
