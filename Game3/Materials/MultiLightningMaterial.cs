﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Materials
{
    public class MultiLightingMaterial : Material
    {
        public Vector3 AmbientColor { get; set; }
        public Vector3[] LightDirection { get; set; }
        public Vector3[] LightColor { get; set; }
        public Vector3 SpecularColor { get; set; }
        public Vector3 DiffuseColor { get; set; }
        public float SpecularPower { get; set; }
        public bool TextureEnabled { get; set; }

        public MultiLightingMaterial()
        {
            AmbientColor = new Vector3(.1f, .1f, .1f);
            LightDirection = new Vector3[2];
            LightColor = new Vector3[] {Vector3.One, Vector3.One };
            SpecularColor = new Vector3(1, 1, 1);
            SpecularPower = 32;
            DiffuseColor = Color.White.ToVector3();
            //TextureEnabled = false;
        }

        public override void SetEffectParameters(Effect effect)
        {
            if (effect.Parameters["AmbientColor"] != null)
                effect.Parameters["AmbientColor"].SetValue(AmbientColor);
            if (effect.Parameters["LightDirection"] != null)
                effect.Parameters["LightDirection"].SetValue(LightDirection);
            if (effect.Parameters["LightColor"] != null)
                effect.Parameters["LightColor"].SetValue(LightColor);
            if (effect.Parameters["SpecularColor"] != null)
                effect.Parameters["SpecularColor"].SetValue(SpecularColor);
            if (effect.Parameters["SpecularPower"] != null)
                effect.Parameters["SpecularPower"].SetValue(SpecularPower);
            if (effect.Parameters["DiffuseColor"] != null)
                effect.Parameters["DiffuseColor"].SetValue(DiffuseColor);
        }
    }
}
