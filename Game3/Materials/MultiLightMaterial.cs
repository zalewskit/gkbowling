﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Materials
{
    class MultiLightMaterial : Material
    {
        public Vector3[] LightDirection { get; set; }
        public Vector3[] LightPosition { get; set; }
        public bool FogEnabled { get; set; }
        public Vector3 CameraPosition { get; set; }
        public bool ShininessEnabled { get; set; }
        public Vector3 MainColor
        {
            get
            {
                return mainColor;
            }
            set
            {
                mainColor = value;
                ColoringEnabled = true;
            }
        }
        private Vector3 mainColor;
        private bool ColoringEnabled;

        public MultiLightMaterial()
        {
            LightDirection = new Vector3[2];
            LightPosition = new Vector3[] { Vector3.UnitY, Vector3.UnitY };
        }

        public override void SetEffectParameters(Effect effect)
        {
            if (effect.Parameters["CameraPosition"] != null)
                effect.Parameters["CameraPosition"].SetValue(CameraPosition);
            if (effect.Parameters["LightDirection"] != null)
                effect.Parameters["LightDirection"].SetValue(LightDirection);
            if (effect.Parameters["LightPosition"] != null)
                effect.Parameters["LightPosition"].SetValue(LightPosition);
            if (effect.Parameters["FogEnabled"] != null)
                effect.Parameters["FogEnabled"].SetValue(FogEnabled);
            if (effect.Parameters["MainColor"] != null)
                effect.Parameters["MainColor"].SetValue(MainColor);
            if (effect.Parameters["ColoringEnabled"] != null)
                effect.Parameters["ColoringEnabled"].SetValue(ColoringEnabled);
            if (effect.Parameters["ShininessEnabled"] != null)
                effect.Parameters["ShininessEnabled"].SetValue(ShininessEnabled);

        }
    }
}
