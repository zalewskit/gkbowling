﻿using Bowling.Cameras;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Materials
{
    class MultiSpotLightMaterial : Material
    {
        public const int LIGHTS_COUNT = 3;

        public Vector3 AmbientLightColor { get; set; }

        public Vector3 DiffuseColor { get; set; }

        public Vector3[] LightPosition { get; set; } = new Vector3[LIGHTS_COUNT];

        public Vector3[] LightDirection { get; set; } = new Vector3[LIGHTS_COUNT];

        public Vector3[] LightColor { get; set; } = new Vector3[LIGHTS_COUNT];

        public float[] LightFalloff { get; set; } = new float[LIGHTS_COUNT];

        public float ConeAngle { get; set; }

        public float MovableLightAngleZ { get; set; } = 45;

        public float MovableLightAngleX { get; set; } = 45;

        public bool FogEnabled { get; set; } = false;


        public MultiSpotLightMaterial()
        {
            AmbientLightColor = new Vector3(.15f, .15f, .15f);
            DiffuseColor = new Vector3(.85f, .85f, .85f);
            ConeAngle = 90;

            LightPosition[0] = new Vector3(11.25f, 3.5f, 5.15f);
            LightDirection[0] = new Vector3(0, -MathHelper.ToRadians(45), 0f);
            LightColor[0] = Color.Blue.ToVector3();
            LightFalloff[0] = 2f;

            LightPosition[1] = new Vector3(7.25f, 2.5f, 10.15f);
            LightDirection[1] = new Vector3(MathHelper.ToRadians(MovableLightAngleX), -MathHelper.ToRadians(90), MathHelper.ToRadians(MovableLightAngleZ));
            LightColor[1] = Color.Red.ToVector3();
            LightFalloff[1] = 8f;

            LightPosition[2] = new Vector3(5.25f, 25.5f, 12.15f);
            LightDirection[2] = new Vector3(0, -MathHelper.ToRadians(90), 0f);
            LightColor[2] = new Color(45, 45, 128, 255).ToVector3();
            LightFalloff[2] = 2f;
        }

        public override void SetEffectParameters(Effect effect)
        {
            if (effect.Parameters["AmbientLightColor"] != null)
                effect.Parameters["AmbientLightColor"].SetValue(AmbientLightColor);
            if (effect.Parameters["DiffuseColor"] != null)
                effect.Parameters["DiffuseColor"].SetValue(DiffuseColor);
            if (effect.Parameters["LightDirection"] != null)
                effect.Parameters["LightDirection"].SetValue(LightDirection);
            if (effect.Parameters["LightColor"] != null)
                effect.Parameters["LightColor"].SetValue(LightColor);
            if (effect.Parameters["LightPosition"] != null)
                effect.Parameters["LightPosition"].SetValue(LightPosition);
            if (effect.Parameters["LightFalloff"] != null)
                effect.Parameters["LightFalloff"].SetValue(LightFalloff);
            if (effect.Parameters["FogEnabled"] != null)
                effect.Parameters["FogEnabled"].SetValue(FogEnabled);
            if (effect.Parameters["ConeAngle"] != null)
                effect.Parameters["ConeAngle"].SetValue(MathHelper.ToRadians(ConeAngle / 2));
        }

        public void UpdateCameraPositionInfo(Camera camera, Effect effect)
        {
            if (effect.Parameters["CameraPosition"] != null)
                effect.Parameters["CameraPosition"].SetValue(camera.Position);
        }
    }
}
