﻿using Bowling.Cameras;
using Bowling.Materials;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Projekt4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bowling
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Skybox skybox;

        List<CModel> sceneModels = new List<CModel>();
        List<CModel> pins = new List<CModel>();
        CModel bowlingBall, basicFloor;
        Camera camera, chaseCamera, targetCamera, fixedCamera, freeCamera;
        MouseState lastMouseState;

        bool isFixedCamera = false;

        Vector3 lightBeginPoint = new Vector3(7.25f, 2.5f, 12.15f);
        Vector3 lightEndPoint = new Vector3(7.25f, 2.5f, 7f);
        bool isLightComingToEndPoint = true;

        Vector3 fixedCameraPosition = new Vector3(20, 5, 16);
        Vector3 fixedCameraTarget = new Vector3(10, 1, 12);
        Vector3 initialBallPosition = new Vector3(11.25f, 0.25f, 17);
        Vector3 endBallPosition = new Vector3(11.25f, 0.25f, 4f);

        private bool isDay;
        private bool isKeyPressed;
        private bool isAnimationPending;

        Effect spotlightEffect;

        MultiSpotLightMaterial spotlightMaterial = new MultiSpotLightMaterial();

        Effect dayLightEffect;
        private bool isPhong = true;
        private Effect spotlightEffectGouraud;
        private Effect dayLightEffectGouraud;
        private bool needsRestart;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.GraphicsProfile = GraphicsProfile.HiDef;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            lastMouseState = Mouse.GetState();
            spotlightEffect = Content.Load<Effect>("Effects/MultipleSpotLightsEffect");
            dayLightEffect = Content.Load<Effect>("Effects/MultipleSpotLightsEffect");
            spotlightEffectGouraud = Content.Load<Effect>("Effects/MultipleSpotLightsEffectGouraud");
            dayLightEffectGouraud = Content.Load<Effect>("Effects/MultipleSpotLightsEffectGouraud");

            isDay = false;
            skybox = new Skybox("Skyboxes/NightSky", Content);

            AddFloor();
            AddPins();
            AddSceneModels();
            AddBall();
            PrepareCameras();
            camera = chaseCamera;
        }

        private void AddFloor()
        {
            //var floorTexture = Content.Load<Texture2D>("Textures/WoodTexture");
            basicFloor = new CModel(Content.Load<Model>("Models/BasicFloor"), new Vector3(0, -4.05f, 7), Vector3.Zero, new Vector3(0.04f, 0.04f, 0.05f), GraphicsDevice) { };

            //basicFloor.Material = spotlightMaterial;
            //basicFloor.SetModelEffect(spotlightEffect, false);
            var material = spotlightMaterial;
            var effect = Content.Load<Effect>("Effects/MultipleSpotLightsEffect");

            basicFloor.Material = material;
            basicFloor.SetModelEffect(effect, false);
        }

        private void AddBall()
        {
            bowlingBall = new CModel(Content.Load<Model>("Models/BowlingBall"), initialBallPosition, new Vector3(MathHelper.ToRadians(0), -MathHelper.ToRadians(0), 0), new Vector3(0.2f), GraphicsDevice);
            initialBallPosition.Y = bowlingBall.BoundingSphere.Radius;
            bowlingBall.Position = initialBallPosition;
            bowlingBall.Material = spotlightMaterial;
            bowlingBall.SetModelEffect(spotlightEffect, true);
        }

        private void PrepareCameras()
        {
            chaseCamera = new ChaseCamera(new Vector3(0, 0.8f, 2.5f), new Vector3(0, 0f, 0), Vector3.Zero, GraphicsDevice);
            targetCamera = new TargetCamera(fixedCameraPosition, bowlingBall.Position, GraphicsDevice);
            fixedCamera = new TargetCamera(fixedCameraPosition, fixedCameraTarget, GraphicsDevice);
            freeCamera = new FreeCamera(new Vector3(10, 5, 10), 0, 0, GraphicsDevice);
        }

        private void AddSceneModels()
        {
            sceneModels = new List<CModel>()
            {
                new CModel(Content.Load<Model>("Models/BowlingTrophy"), new Vector3(4,-0.1f,4), new Vector3(-MathHelper.ToRadians(90), -MathHelper.ToRadians(45),0), new Vector3(0.1f), GraphicsDevice),
                new CModel(Content.Load<Model>("Models/Table"), new Vector3(4,0.9f,14), new Vector3(0, 0,0), new Vector3(0.02f), GraphicsDevice),
                new CModel(Content.Load<Model>("Models/BowlingBowl"), new Vector3(13,0,17), new Vector3(0, MathHelper.ToRadians(15),0), new Vector3(1.05f), GraphicsDevice),
            };

            sceneModels.Add(new CModel(Content.Load<Model>("Models/Table"), new Vector3(4, 0.9f, 20), new Vector3(0, 0, 0), new Vector3(0.02f), GraphicsDevice));

            Effect someEffect = Content.Load<Effect>("Effects/TextureMapping");
            LightingMaterial material = new LightingMaterial()
            {
            };

            foreach (var model in sceneModels)
            {
                model.Material = spotlightMaterial;
                model.SetModelEffect(spotlightEffect, true);
            }
        }

        private void AddPins()
        {
            pins = new List<CModel>()
            {
                new CModel(Content.Load<Model>("Models/BowlingPin"), new Vector3(11, 0f, 5), new Vector3(0, 0, 0), new Vector3(0.05f), GraphicsDevice),
                new CModel(Content.Load<Model>("Models/BowlingPin"), new Vector3(11.5f, 0f, 5), new Vector3(0, 0, 0), new Vector3(0.05f), GraphicsDevice),
                new CModel(Content.Load<Model>("Models/BowlingPin"), new Vector3(11.25f, 0f, 5.3f), new Vector3(0, 0, 0), new Vector3(0.05f), GraphicsDevice)
            };

            LightingMaterial material = new LightingMaterial()
            {
                TextureEnabled = true,
            };

            Effect someEffect = Content.Load<Effect>("Effects/TextureMapping");
            pins[0].Material = spotlightMaterial;
            pins[0].SetModelEffect(spotlightEffect, true);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            UpdateSettings(gameTime);
            UpdateModel(gameTime);
            UpdateCamera(gameTime);
            UpdateLights(gameTime);
            base.Update(gameTime);
        }

        private void UpdateLights(GameTime gameTime)
        {
            float lightStep = 3 * (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (isLightComingToEndPoint)
                spotlightMaterial.LightPosition[1].Z -= lightStep;
            else
                spotlightMaterial.LightPosition[1].Z += lightStep;

            if (spotlightMaterial.LightPosition[1].Z <= lightEndPoint.Z)
                isLightComingToEndPoint = false;

            if (spotlightMaterial.LightPosition[1].Z > lightBeginPoint.Z)
                isLightComingToEndPoint = true;
        }

        private void UpdateCamera(GameTime gameTime)
        {
            switch (camera)
            {
                case FreeCamera c:
                    UpdateFreeCamera(gameTime);
                    break;
                case ChaseCamera c:
                    UpdateChaseCamera(gameTime);
                    break;
                case TargetCamera c:
                    UpdateTargetCamera(gameTime);
                    break;
                default:
                    break;

            }
        }

        private void UpdateTargetCamera(GameTime gameTime)
        {
            if (isFixedCamera)
            {
                ((TargetCamera)camera).Target = fixedCameraTarget;
            }
            else
            {
                ((TargetCamera)camera).Target = bowlingBall.Position;
            }
            camera.Update();
        }

        private void UpdateSettings(GameTime gameTime)
        {
            var keyState = Keyboard.GetState();

            if (keyState.IsKeyDown(Keys.F1))
            {
                camera = chaseCamera;
            }
            if (keyState.IsKeyDown(Keys.F2))
            {
                camera = targetCamera;
                isFixedCamera = false;
            }
            if (keyState.IsKeyDown(Keys.F3))
            {
                camera = fixedCamera;
                isFixedCamera = true;
            }
            if (keyState.IsKeyDown(Keys.F4))
            {
                camera = freeCamera;
            }
            if (keyState.IsKeyDown(Keys.R))
            {
                needsRestart = true;
                RestartModelsInitialState();
                needsRestart = false;
            }
            if (keyState.IsKeyDown(Keys.N) && !isKeyPressed)
            {
                ChangeDayNightSettings();
                isKeyPressed = true;
            }
            if (keyState.IsKeyDown(Keys.F) && !isKeyPressed)
            {
                spotlightMaterial.FogEnabled = !spotlightMaterial.FogEnabled;
                isKeyPressed = true;
            }
            if (keyState.IsKeyDown(Keys.G) && !isKeyPressed)
            {
                isPhong = !isPhong;
                ChangeDayNightSettings();
                ChangeDayNightSettings();
                isKeyPressed = true;
            }
            if (keyState.IsKeyUp(Keys.N)
                && keyState.IsKeyUp(Keys.R)
                && keyState.IsKeyUp(Keys.F)
                && keyState.IsKeyUp(Keys.G))
            {
                isKeyPressed = false;
            }

            if (keyState.IsKeyDown(Keys.Up))
            {
                if (spotlightMaterial.MovableLightAngleZ < 90)
                    spotlightMaterial.MovableLightAngleZ++;
                spotlightMaterial.LightDirection[1].Z = -MathHelper.ToRadians(spotlightMaterial.MovableLightAngleZ);
            }
            if (keyState.IsKeyDown(Keys.Down))
            {
                if (spotlightMaterial.MovableLightAngleZ > -90)
                    spotlightMaterial.MovableLightAngleZ--;
                spotlightMaterial.LightDirection[1].Z = -MathHelper.ToRadians(spotlightMaterial.MovableLightAngleZ);
            }

            if (keyState.IsKeyDown(Keys.Left))
            {
                if (spotlightMaterial.MovableLightAngleX < 90)
                    spotlightMaterial.MovableLightAngleX++;
                spotlightMaterial.LightDirection[1].X = -MathHelper.ToRadians(spotlightMaterial.MovableLightAngleX);
            }
            if (keyState.IsKeyDown(Keys.Right))
            {
                if (spotlightMaterial.MovableLightAngleX > -90)
                    spotlightMaterial.MovableLightAngleX--;
                spotlightMaterial.LightDirection[1].X = -MathHelper.ToRadians(spotlightMaterial.MovableLightAngleX);
            }
            if (keyState.IsKeyDown(Keys.Space))
            {
                isAnimationPending = true;
            }
        }

        private void UpdateModel(GameTime gameTime)
        {
            KeyboardState keyState = Keyboard.GetState();

            if (!isAnimationPending)
            {
                return;
            }
            var rot = new Vector3(-1, 0, 0);

            bowlingBall.Rotation += rot * 0.2f;
            bowlingBall.Position += Vector3.Forward * 5f * (float)gameTime.ElapsedGameTime.TotalSeconds; ;
            if (bowlingBall.Position.Z < endBallPosition.Z)
            {
                bowlingBall.Scale = Vector3.Zero;
                isAnimationPending = false;
                needsRestart = true;
                Task.Delay(2000).ContinueWith((t) =>
                {
                    RestartModelsInitialState();
                });
            }
            pins.Where(p => p.Position.Z > bowlingBall.Position.Z)
                .ToList()
                .ForEach(pin => pin.Scale = Vector3.Zero);
        }
        
        private void RestartModelsInitialState()
        {
            if (!needsRestart)
                return;

            isAnimationPending = false;
            bowlingBall.Position = initialBallPosition;
            bowlingBall.Scale = new Vector3(0.2f);
            foreach (var pin in pins)
                pin.Scale = new Vector3(0.05f);
            needsRestart = false;
        }

        private void UpdateChaseCamera(GameTime gameTime)
        {
            ((ChaseCamera)camera).Move(bowlingBall.Position, Vector3.Zero);
            camera.Update();
        }

        private void UpdateFreeCamera(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            KeyboardState keyState = Keyboard.GetState();

            float deltaX = (float)lastMouseState.X - (float)mouseState.X;
            float deltaY = (float)lastMouseState.Y - (float)mouseState.Y;

            ((FreeCamera)camera).Rotate(deltaX * .005f, deltaY * .005f);
            Vector3 translation = Vector3.Zero;

            if (keyState.IsKeyDown(Keys.W)) translation += Vector3.Forward;
            if (keyState.IsKeyDown(Keys.S)) translation += Vector3.Backward;
            if (keyState.IsKeyDown(Keys.A)) translation += Vector3.Left;
            if (keyState.IsKeyDown(Keys.D)) translation += Vector3.Right;

            translation *= 20f * (float)gameTime.ElapsedGameTime.TotalSeconds;
            ((FreeCamera)camera).Move(translation);
            camera.Update();
            lastMouseState = mouseState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            RasterizerState rasterizerState = new RasterizerState();
            rasterizerState.CullMode = CullMode.None;
            GraphicsDevice.RasterizerState = rasterizerState;

            DrawModels();

            bowlingBall.Draw(camera.View, camera.Projection, camera.Position);
            basicFloor.Draw(camera.View, camera.Projection, camera.Position);

            //rasterizerState.CullMode = CullMode.CullClockwiseFace;
            skybox.Draw(camera.View, camera.Projection, camera.Position);
            //rasterizerState.CullMode = CullMode.CullCounterClockwiseFace;

            // floor.Draw(camera, floorEffect);
            //terrain.Draw(camera);
            //sphere.Draw(camera);
            base.Draw(gameTime);
        }

        private void DrawModels()
        {
            foreach (var pin in pins)
            {
                pin.Draw(camera.View, camera.Projection, camera.Position);
            }

            foreach (var model in sceneModels)
            {
                model.Draw(camera.View, camera.Projection, camera.Position);
            }
        }

        private void ChangeDayNightSettings()
        {
            if (isDay)
            {
                skybox = new Skybox("Skyboxes/NightSky", Content);
                if(isPhong)
                {
                    SetEffects(spotlightMaterial, spotlightEffect);
                }
                else
                {
                    SetEffects(spotlightMaterial, spotlightEffectGouraud);
                }
                spotlightMaterial.LightColor[2] = new Color(45, 45, 128, 255).ToVector3();
                isDay = false;
            }
            else
            {
                skybox = new Skybox("Skyboxes/SkyBox", Content);
                if (isPhong)
                {
                    SetEffects(spotlightMaterial, spotlightEffect);
                }
                else
                {
                    SetEffects(spotlightMaterial, spotlightEffectGouraud);
                }
                spotlightMaterial.LightColor[2] = Color.LightYellow.ToVector3();
                isDay = true;
            }
        }

        private void SetEffects(Material material, Effect effect)
        {
            foreach (var model in sceneModels)
            {
                model.Material = material;
                model.SetModelEffect(effect, true);
            }

            foreach (var pin in pins)
            {
                pin.Material = material;
                pin.SetModelEffect(effect, true);
            }

            bowlingBall.Material = material;
            bowlingBall.SetModelEffect(effect, true);

            basicFloor.Material = material;
            basicFloor.SetModelEffect(effect, true);
        }
    }
}
