#define SPOTLIGHTS_NUM 2

float4x4 World;
float4x4 View;
float4x4 Projection;

float3 CameraPosition;

float3 AmbientLightColor = float3(.20, .20, .20);
float3 DiffuseColor = float3(.85, .85, .85);
float3 LightPosition[SPOTLIGHTS_NUM];
float3 LightDirection[SPOTLIGHTS_NUM];
float ConeAngle = 1;
float3 LightColor = float3(1, 1, 1);
float LightFalloff = 20;
float SpecularPower = 32;
float3 SpecularColor = float3(1, 1, 1);
bool ShininessEnabled = true;

bool ColoringEnabled = false;
float3 MainColor = float3(1, 1, 1);

bool FogEnabled = false;
float FogStart = 10;
float FogEnd = 300;
float3 FogColor = float3(1, 1, 1);
//spotlight x2

struct VertexShaderInput
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;
};
struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float3 Normal : TEXCOORD1;
    float4 Color : COLOR0;
};

float3 DiffuseLighting(int lightIndex, float3 normal, float4 worldPosition)
{
    float3 lightDir = normalize(LightPosition[lightIndex] - worldPosition);
    float diffuse = saturate(dot(normalize(normal), lightDir));
    float d = dot(-lightDir, normalize(LightDirection[lightIndex]));
    float a = cos(ConeAngle);
    float att = 0;
    if (a < d)
        att = 1 - pow(clamp(a / d, 0, 1), LightFalloff);
    return diffuse * att * LightColor;
}

float3 SpecularLighting(int lightIndex, float3 normal, float4 worldPosition, float3 viewDirection)
{
    float3 lightDir = normalize(LightPosition[lightIndex] - worldPosition);
    float3 refl = reflect(lightDir, normalize(normal));
    float3 view = normalize(viewDirection);

    return pow(saturate(dot(refl, view)), SpecularPower) * SpecularColor;
}

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
    output.Normal = mul(input.Normal, World);
    float3 viewDirection = worldPosition - CameraPosition;

    float3 diffuseColor = DiffuseColor;
    if (ColoringEnabled)
        diffuseColor *= MainColor;

    float3 totalLight = AmbientLightColor * diffuseColor;

    for (int i = 0; i < SPOTLIGHTS_NUM; i++)
    {
        totalLight += DiffuseLighting(i, input.Normal, worldPosition) * diffuseColor;
    }

    float3 finalColor = totalLight;
    if (FogEnabled)
    {
        float dist = length(viewDirection);
        float fog = clamp((dist - FogStart) / (FogEnd - FogStart), 0, 1);
        finalColor = lerp(finalColor, FogColor, fog);
    }
    output.Color = float4(finalColor, 1);
    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    return input.Color;
}

technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_4_0_level_9_1 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}