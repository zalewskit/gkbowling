#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_5_0
	#define PS_SHADERMODEL ps_5_0
#endif
#define NUMLIGHTS 3

float4x4 World;
float4x4 View;
float4x4 Projection;
float3 CameraPosition;

float3 AmbientLightColor = float3(.15, .15, .15);
float3 DiffuseColor = float3(.85, .85, .85);
float3 LightPosition[NUMLIGHTS];
float3 LightDirection[NUMLIGHTS];
float ConeAngle = 90;
float LightAttenuation = 5000;
float3 LightColor[NUMLIGHTS];
float LightFalloff[NUMLIGHTS];

bool TextureEnabled = false;
bool FogEnabled;

texture BasicTexture;

sampler BasicTextureSampler = sampler_state
{
    texture = <BasicTexture>;
    MinFilter = Anisotropic; // Minification Filter
    MagFilter = Anisotropic; // Magnification Filter  
    MipFilter = Linear; // Mip-mapping
    AddressU = Wrap; // Address Mode for U Coordinates
    AddressV = Wrap; // Address Mode for V Coordinates
};


struct VertexShaderInput
{
    float4 Position : SV_Position;
    float3 Normal : NORMAL0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 UV : TEXCOORD0;
    float3 Normal : TEXCOORD1;
    float4 WorldPosition : TEXCOORD2;
    float3 ViewDirection : TEXCOORD3;
    float4 Color : COLOR0;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    float3 viewDirection = worldPosition - CameraPosition;

    output.Position = mul(viewPosition, Projection);
    output.WorldPosition = worldPosition;
    output.Normal = mul(input.Normal, World);
    output.ViewDirection = viewDirection;

    float3 color = DiffuseColor;
    float3 lighting = AmbientLightColor;

    [loop]
    for (int i = 0; i < NUMLIGHTS; i++)
    {
        float3 lightDir = normalize(LightPosition[i] - output.WorldPosition);
        float diffuse = saturate(dot(normalize(output.Normal), lightDir));
        float d = dot(-lightDir, normalize(LightDirection[i]));
        float a = cos(ConeAngle);
        float att = 0;
        if (a < d)
            att = 1 - pow(clamp(a / d, 0, 1), LightFalloff[i]);
        lighting += (diffuse * att * LightColor[i]);
    }

    //lighting = saturate(lighting) * color;
    float FogStart = 1.5f;
    float FogEnd = 15;
    float3 FogColor = float3(1, 1, 1);
    if (FogEnabled)
    {
        float dist = length(output.ViewDirection);
        float fog = clamp((dist - FogStart) / (FogEnd - FogStart), 0, 1);
        lighting = lerp(lighting, FogColor, fog);
    }


    output.Color = float4(saturate(lighting) * color, 1);
    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    //float3 color = DiffuseColor;
    //float3 lighting = AmbientLightColor;

    //[loop]
    //for(int i=0; i<NUMLIGHTS; i++)
    //{
    //    float3 lightDir = normalize(LightPosition[i] - input.WorldPosition);
    //    float diffuse = saturate(dot(normalize(input.Normal), lightDir));
    //    float d = dot(-lightDir, normalize(LightDirection[i]));
    //    float a = cos(ConeAngle);
    //    float att = 0;  
    //    if (a < d)
    //        att = 1 - pow(clamp(a / d, 0, 1), LightFalloff[i]);
    //    lighting += (diffuse * att * LightColor[i]);
    //}

    ////lighting = saturate(lighting) * color;
    //float FogStart = 1.5f;
    //float FogEnd = 15;
    //float3 FogColor = float3(1, 1, 1);
    //if (FogEnabled)
    //{
    //    float dist = length(input.ViewDirection);
    //    float fog = clamp((dist - FogStart) / (FogEnd - FogStart), 0, 1);
    //    lighting = lerp(lighting, FogColor, fog);
    //}


    //return float4(saturate(lighting) * color, 1);

    return input.Color;
}

technique Technique1
{
    pass Pass1
    {
        VertexShader = compile VS_SHADERMODEL VertexShaderFunction();
        PixelShader = compile PS_SHADERMODEL PixelShaderFunction();
    }
}
